from django.db import models

# Create your models here.


class Bloguser(models.Model):
    nick_name = models.CharField(max_length=200, unique=True)
    mobile_number = models.CharField(max_length=200)
    email = models.CharField(max_length=200,unique=True)
    # checkemail = models.BooleanField(False)
    password = models.CharField(max_length=200)
    c_time = models.DateTimeField(auto_now_add=True)
    has_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.nick_name

    class Meta:
        ordering = ["-c_time"]
        verbose_name = "用户"
        verbose_name_plural = "用户"

class ConfirmString(models.Model):
    #ConfirmString模型保存了用户和注册码之间的关系，一对一的形式；
    code = models.CharField(max_length=256)#code字段是哈希后的注册码
    user = models.OneToOneField('Bloguser', on_delete=models.CASCADE)
    c_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.nick_name + ":   " + self.code

    class Meta:

        ordering = ["-c_time"]
        verbose_name = "确认码"
        verbose_name_plural = "确认码"


class Article(models.Model):
    category = models.CharField(max_length=100,verbose_name="分类",null=False)
    title = models.CharField(max_length=100,verbose_name='标题')
    author = models.CharField(max_length=50,verbose_name="作者")
    abstract = models.CharField(max_length=500,verbose_name="摘要",null=False)
    content = models.TextField(verbose_name="文本内容")
    c_time = models.DateTimeField(auto_now_add=True,verbose_name="创建日期")
    like_num = models.IntegerField(verbose_name="点赞数",default=0)
    reply_num = models.IntegerField(verbose_name="回复数",default=0)
    read_num = models.IntegerField(verbose_name="阅读量",default=0)
    class Meta:
        verbose_name_plural = "文章发布"
        verbose_name = "文章发布"

class LogInfo(models.Model):
    types_dict_choice = [(0,"阅读"),(1,"点赞"),(2,"拍砖")]
    types = models.IntegerField(verbose_name="分类",choices=types_dict_choice,default=0)
    user = models.ForeignKey(Bloguser,on_delete=models.CASCADE)
    c_time = models.DateTimeField(auto_now_add=True,verbose_name="创建日期")
    class Meta:
        verbose_name_plural = "点评信息"
        verbose_name = "点评信息"

class CommentInfo(models.Model):
    user = models.ForeignKey(Bloguser, on_delete=models.CASCADE)
    article = models.ForeignKey(Article,on_delete=models.CASCADE)
    c_time = models.DateTimeField(auto_now_add=True,verbose_name="创建日期")
    content = models.TextField(verbose_name="文本内容")
    class Meta:
        verbose_name_plural = "文章评论"
        verbose_name = "文章评论"





