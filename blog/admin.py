from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from blog.models import Bloguser, Article
from . import models

# 创建一个ModelAdmin的子类
class BaseAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '../plugins/jquery1.12.4.min.js',
            'js/admin.js',
        )

class UserAdimin(BaseAdmin):
    list_display = ["nick_name","mobile_number","email","c_time"]
    fields = ["nick_name","mobile_number","email"]

class ArticleAdmin(BaseAdmin):
    list_display = ["category","author","title","abstract","like_num","reply_num","read_num","c_time"]
    list_display_links = ["title"]
    fields = ["category","author","title","abstract","content"]
    class Media:
        js = (
            '../static/plugins/kindeditor/kindeditor-all.js',
            '../static/plugins/kindeditor/config.js',
            '../static/plugins/kindeditor/lang/zh-CN.js'
        )
# admin.site.register(models.Bloguser)
admin.site.register(models.ConfirmString)

# 注册的时候，将原模型和ModelAdmin耦合起来
admin.site.register(Bloguser,UserAdimin)
admin.site.register(Article,ArticleAdmin)