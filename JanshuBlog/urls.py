"""JanshuBlog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from JanshuBlog import settings
from blog import views

app_name ="blog"

urlpatterns = [
    path('admin/',admin.site.urls),
    path('',views.index,name="index"),
    path('home/',include('blog.urls')),
    url(r'^login/',views.login),
    url(r'^register/',views.register),
    url(r'^logout/',views.logout),
    url(r'^captcha',include('captcha.urls')),
    path('confirm/',views.user_confirm),
    path('upload_image',views.upload_image,name='upload_image'),#图片上传路由
    re_path(r"^upload_image/(?P<path>.*)", serve, {"document_root": settings.MEDIA_ROOT}),
    re_path(r"^upload_image/(?P<path>.*)", serve, {"document_root": os.path.join(settings.MEDIA_ROOT, "image")}),
    path('index_more',views.get_index_more,name="index_more"),
    path('detail/<int:nid>', views.detail),
]
